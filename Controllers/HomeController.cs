﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {
        private Person[] personData =
        {
            new Person
            {
                PersonId = 1,
                FirstName = "Adam",
                LastName = "Freeman",
                Role = Role.Admin
            },
            new Person {
                PersonId = 2,
                FirstName = "Jacqui",
                LastName = "Griffyth",
                Role = Role.User
            },
            new Person
            {
                PersonId = 3,
                FirstName = "John",
                LastName = "Smith",
                Role = Role.User
            },
            new Person {
                PersonId = 4,
                FirstName = "Anne",
                LastName = "Jones",
                Role = Role.Guest
            }
        };
        
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        /*public IActionResult Index()
        {
            return View();
        }*/

        public ActionResult Index(int id = 1)
        {
            Person dataItem = personData.Where(p => p.PersonId == id).First();
            return View(dataItem);
        }

        public ActionResult CreatePerson()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult CreatePerson(Person model)
        {
            return View("Index", model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}