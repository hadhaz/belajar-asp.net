﻿using Microsoft.AspNetCore.Mvc;

namespace WebApplication5.Controllers
{
    public class HackThisWebsite : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Controller = "Hack This Website";
            ViewBag.Action = "The Action is Index";
            ViewBag.Dummy = "TEST";
            return View("Hello");
        }
    }
}
